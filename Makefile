download_zig:
	wget https://ziglang.org/download/0.12.0/zig-linux-x86_64-0.12.0.tar.xz

extract_zig:
	tar xf zig-linux-x86_64-0.12.0.tar.xz
    
install_for_bash:
	echo "export PATH=\$$PATH:$$PWD/zig-linux-x86_64-0.12.0" >> ~/.bashrc
	curl -LO "https://raw.githubusercontent.com/ziglang/shell-completions/master/_zig.bash"
	echo ". $$PWD/_zig.bash" >> ~/.bashrc

install_for_zsh:
	echo "export PATH=\$$PATH:$$PWD/zig-linux-x86_64-0.12.0" >> ~/.zshrc
	curl -LO "https://raw.githubusercontent.com/ziglang/shell-completions/master/_zig"
	mkdir -p zsh_completion
	mv _zig zsh_completion
	echo "fpath=($$PWD/zsh_completion \$$fpath)" >> ~/.zshrc
	@printf "\nIMPORTANT\nopen new zsh and run 'compinit' now\n"

